//
//  ImageDownloadManager.swift
//  ImageCacher
//
//  Created by Babul on 13/07/16.
//  Copyright © 2016 Babul. All rights reserved.
//

import UIKit
import ObjectiveC
///Data manager class to implement caching
public class DataManager:NSObject {
    
    var localCache:NSCache!;//local cache for in memory caching
    override init() {
        self.localCache = NSCache();
        self.localCache.totalCostLimit = 30;//max memory caching set to 30 considering each object with weight 1
    }
    
    
    //cache image with corresponding key
  public  func cacheImage(image:UIImage,forKey key:String) -> Void {
        self.localCache.setObject(image, forKey: key, cost:1);
    }
    
    
    //return cached image if found else nil
   public func getCachedImage(forKey key:String) -> UIImage? {
        return self.localCache.objectForKey(key) as? UIImage ;
    }
    
    //cache data with corresponding key
   public func cacheData(data:AnyObject,forKey key:String)  {
          self.localCache.setObject(data, forKey: key , cost: 1);
    }
    
     //return cached data if found else nil
   public func getCachedData(forKey key:String) -> AnyObject? {
        return self.localCache.objectForKey(key)  ;
    }
    
    
}


//Data Download manager
//manages all api calls with managment to give data from cache or from network
public class DownloadManager: NSObject {

    let operationQueue : NSOperationQueue! // being used in HttpClient Util for managing queue
   public let dataManager = DataManager();
    
    /// singleton instance
    public static let sharedInstance  = DownloadManager()
    private override init() {
        self.operationQueue = NSOperationQueue();
        self.operationQueue.maxConcurrentOperationCount = 10;
       
    
    }
    
    
    
    
    
    
    ///Download JSON data from url 
    //parse it to JSON
    
   public func downloadJSON(urlStr:String,callback:(obj:NSHTTPURLResponse?,data: AnyObject?)->Void) -> NSURLSessionDataTask? {
       //checks if data present in cache 
        //return if found else get data from network
        var jsonResponse = self.dataManager.getCachedData(forKey: urlStr);
        if jsonResponse != nil {
            callback(obj: nil,data: jsonResponse);
            return nil;
            
        } else {
    //data not found in cache means network call
            return self.downloadData(urlStr, callback: { (obj, data) in
                //check status code is sucess then save it cache
                if obj?.statusCode == HttpStatusCode.HTTP_SUCCESS_STATUS_CODE.rawValue {
                    jsonResponse = data?.parseToJson();
                    self.dataManager.cacheData(jsonResponse!, forKey: urlStr);
                  
                }
                
                //give callback
                  callback(obj: obj,data: jsonResponse);
                
            })
        
        
        }

    }
    
    
    ///Download Image data from url
    //parse it to Image
   public func downloadImage(urlStr:String,callback:(obj:NSHTTPURLResponse?,data: UIImage?)->Void) -> NSURLSessionDataTask? {
    
    //checks if image present in cache
    //return if found else get image from network
        var img = self.dataManager.getCachedImage(forKey: urlStr);
        if img != nil {
            callback(obj: nil,data: img);
            return nil;
            
        } else {
            //data not found in cache means network call

            return self.downloadData(urlStr, callback: { (obj, data) in
                 //check status code is sucess then save it cache
                if obj?.statusCode == HttpStatusCode.HTTP_SUCCESS_STATUS_CODE.rawValue {
                    img = data?.getImage();
                    if img != nil {
                        self.dataManager.cacheData(img!, forKey: urlStr);
                    }
                }
                //give callback
                callback(obj: obj,data: img);
                
            })
            
            
        }
        
    }

   
    
    
    
    //actuall api calling is done from here
    func downloadData(urlStr:String,callback:(obj:NSHTTPURLResponse?,data: NSData?)->Void) -> NSURLSessionDataTask {
        return HTTPClientUtil().httpGET(urlStr, requestParams: nil, requestHeaders: HTTPClientUtil.requestHeaders(), callback: { (obj, data) in
            callback(obj: obj,data: data);
            
        });
        
    }
    
   
    
    
}

//extension to NSData
extension NSData {
    
    //parse NSData to JSON
    func parseToJson() -> AnyObject? {
          var responseJSON:AnyObject?;
        do {
            responseJSON =  try  NSJSONSerialization.JSONObjectWithData(self, options: NSJSONReadingOptions.AllowFragments) ;
            
        }catch let JSONError as NSError {
            print("\(JSONError)")
        }
        catch {
            print("unknown error in JSON Parsing");
        }
        
        return  responseJSON;
            
       
    }
    
    //parse NSData to UIImage
    func getImage() -> UIImage? {
        return UIImage(data: self);
    }
    
    
}

//Image View Category
private var imgViewAssociationKey: UInt8 = 0
extension UIImageView  {
    //operation in which image is being downloaded
    var operation: NSURLSessionDataTask? {
     
            get {
                return objc_getAssociatedObject(self, &imgViewAssociationKey) as? NSURLSessionDataTask
            }
            set(newValue) {
                objc_setAssociatedObject(self, &imgViewAssociationKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }

    //method to download image from url  asynchronously via callbacks
    func imageWithContentOfUrl(url:String,withPlaceHolderImage image:UIImage) {
        
       
            //download
        cancelDownload(); //cancel previously downloading image on same imageView
        
        //set placeholder image until image is being downloaded
        dispatch_async(dispatch_get_main_queue(), { 
            self.image = image;
             self.setNeedsLayout();
        });
        
        
        //url empty string check
        if url.characters.count <= 0 {
            return;
        }
        
        //download image async.
        self.operation = DownloadManager.sharedInstance.downloadImage(url , callback: { (obj, data) in
            if data != nil {
                //if image downloaded 
                //replace placeholder image to downloaded image
                dispatch_async(dispatch_get_main_queue(), {
                    
                    self.image = data;
                    self.layoutIfNeeded();
                })
               
                
            }
            
            
        });
        
        
        
        
    }
    
    
    func cancelDownload() -> Void {
        
        self.operation?.cancel();
        
        
    }
    
}


