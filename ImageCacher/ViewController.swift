//
//  ViewController.swift
//  ImageCacher
//
//  Created by Babul on 12/07/16.
//  Copyright © 2016 Babul. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var lazyLoader:LazyLoadRequester!;
    var collectionArr:[Movie] = [];
    @IBOutlet weak var collectionView:UICollectionView!;
    override func viewDidLoad() {
        super.viewDidLoad()
       self.title = "Image Browser";
        lazyLoader = LazyLoadRequester(queryString: "funny");
        
        
        lazyLoader.getImagesList { (movies) in
            dispatch_async(dispatch_get_main_queue(), {
                // code here
                self.collectionArr = movies;
                self.collectionView.reloadData();
            })
           
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK - Collection View Data sources - 
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as! ImageCollectionCell;
        
        
        let movie = self.collectionArr[indexPath.row];
        
        cell.imgView.imageWithContentOfUrl(movie.poster.thumbStr ?? "", withPlaceHolderImage: UIImage(named: "image_placeholder")!);
        
        return cell;
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionArr.count;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake((self.view.bounds.width/2 - 10), (self.view.bounds.width/2 - 10));
        
    }
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset;
        let bounds = scrollView.bounds;
        let size = scrollView.contentSize;
        let inset = scrollView.contentInset;
        let y = offset.y + bounds.size.height - inset.bottom;
        let h = size.height;
        if(y > h &&  self.lazyLoader.isPending == false) {
            self.lazyLoader.incrementCount();
            self.lazyLoader.getImagesList({ (movies) in
                
                dispatch_async(dispatch_get_main_queue(), {
                    // code here
                    var indexPathArr:[NSIndexPath] = [];
                    
                    let row = self.collectionArr.count;
                    
                    for i in 0...(movies.count-1) {
                        
                        indexPathArr.append(NSIndexPath(forRow: (row+i), inSection: 0));
                        
                    }
                    
                    
                    
                    self.collectionArr = self.collectionArr + movies;
                    self.collectionView.performBatchUpdates({
                        
                        self.collectionView.insertItemsAtIndexPaths(indexPathArr);
                        
                        }, completion: nil)
                })
               
                
            })
            
        }
    }
    
    
}


class ImageCollectionCell: UICollectionViewCell {
     @IBOutlet weak var imgView:UIImageView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        //to check if dequeue is working
    }
    
    
  
    
}