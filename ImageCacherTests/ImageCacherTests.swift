//
//  ImageCacherTests.swift
//  ImageCacherTests
//
//  Created by Babul Prabhakar on 16/07/16.
//  Copyright © 2016 belzabar. All rights reserved.
//

import XCTest
import ImageCacher
class ImageCacherTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func doSomethingThatTakesSomeTimes(timeInterval : NSTimeInterval,completion:()->Void) -> Void {
         let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue) { 
            NSThread.sleepForTimeInterval(timeInterval);
            dispatch_sync(dispatch_get_main_queue(), {
                completion();
                });
        }
        
    }
    
   
    
    //test download of JSON from Api
    func testDownloadJSON() {
        var lazyLoader:LazyLoadRequester!;
        lazyLoader = LazyLoadRequester(queryString: "funny");
        //given  
        //  max Data count limit is set to 10
        lazyLoader.mCount = 10;
        
        //when api called
        let completionExpectation = self.expectationWithDescription("Api Call");
        
        self.doSomethingThatTakesSomeTimes(4) { 
            lazyLoader.getImagesList { (movies) in
                //then api result should return 10 elements
                XCTAssert(movies.count == 10,"movie count must be 10");
                for movie in movies{
                    //movie attributes cannot be nil
                    XCTAssert(movie.title != nil,"movie title cannot be nil");
                    XCTAssert(movie.fanart != nil,"movie title cannot be nil");
                    XCTAssert(movie.poster != nil,"movie title cannot be nil");
                }
                
                completionExpectation.fulfill();
            }
        }
        
        self.waitForExpectationsWithTimeout(6, handler: nil);
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    //test caching of JSON  
    func testCachingOfJSON() {
        
        //given
        //first mock data that will be stored in cache
        
        let datamanger =   DownloadManager.sharedInstance.dataManager;
        
        
        let cacheDict =  [[
            "movie": [
                "title": "Nature",
                "images": [
                    "poster": [
                        "full": "https://walter.trakt.us/images/movies/000/074/069/posters/original/bc5d9a56f5.jpg",
                        "medium": "https://walter.trakt.us/images/movies/000/074/069/posters/medium/bc5d9a56f5.jpg",
                        "thumb": "https://walter.trakt.us/images/movies/000/074/069/posters/thumb/bc5d9a56f5.jpg"
                    ],
                    "fanart": [
                        "full": "https://walter.trakt.us/images/movies/000/074/069/fanarts/original/f8c69abcba.jpg",
                        "medium": "https://walter.trakt.us/images/movies/000/074/069/fanarts/medium/f8c69abcba.jpg",
                        "thumb": "https://walter.trakt.us/images/movies/000/074/069/fanarts/thumb/f8c69abcba.jpg"
                    ]
                ]
                
            ]
            ]];
        
       
        //cache data with respective url
        datamanger.cacheData(cacheDict, forKey: "https://api-v2launch.trakt.tv/search?limit=10&page=1&query=funny&type=movie");
        
         //when
        var lazyLoader:LazyLoadRequester!;
        lazyLoader = LazyLoadRequester(queryString: "funny");
       
        //  max Data count limit is set to 10
        lazyLoader.mCount = 10;
       
        lazyLoader.getImagesList { (movies) in
            //then cache data will be returned
            XCTAssert(movies.count == 1,"movie count must be 1");
            for movie in movies{
                //movie attributes cannot be nil
                XCTAssert(movie.title == "Nature","movie title must be Nature");
                XCTAssert(movie.fanart.thumbStr.compare("https://walter.trakt.us/images/movies/000/074/069/fanarts/thumb/f8c69abcba.jpg") == .OrderedSame );
            }
        }
        
        
        
        //then get data for json will be called and checked if data matches
        
    }
    
    func testDownloadImage() {
        
        //given
        let urlStr = "https://walter.trakt.us/images/movies/000/074/069/fanarts/thumb/f8c69abcba.jpg";
        
        
         //when
        let completionExpectation = self.expectationWithDescription("Api Call");
        self.doSomethingThatTakesSomeTimes(4) {
        DownloadManager.sharedInstance.downloadImage(urlStr) { (obj, data) in
        
            //then
            XCTAssert(obj!.statusCode == 200,"Status code must be 200 for sucess");
            XCTAssert(data != nil,"Image must not be nil");
            completionExpectation.fulfill();
            
        }
        }
       
        
        
        self.waitForExpectationsWithTimeout(6, handler: nil);
        
        
    }
    
    
    func testCacheImage() {
        //given
        //first mock data that will be stored in cache
        
        let datamanger =   DownloadManager.sharedInstance.dataManager;
        let urlStr = "https://walter.trakt.us/images/movies/000/074/069/fanarts/thumb/f8c69abcba.jpg";
        let img = UIImage(named: "image_placeholder")!
        datamanger.cacheImage(img, forKey: urlStr);
        DownloadManager.sharedInstance.downloadImage(urlStr) { (obj, data) in
            
            //then
           
            XCTAssert(img == data,"Images must be same");
            
            
        }
        
        
        
    }
    
    

    
}
