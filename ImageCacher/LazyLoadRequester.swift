//
//  LazyLoadRequester.swift
//  ImageCacher
//
//  Created by Babul on 13/07/16.
//  Copyright © 2016 Babul. All rights reserved.
//

import UIKit
let BASE_URL = "https://api-v2launch.trakt.tv/search";


//Lazy Loader class to download images list with pagination to max count 10
public class LazyLoadRequester: NSObject {
  public  var mCount : Int = 10;
  public  var currentIndex : Int = 1;
  public  var isPending:Bool = false;
  public  var queryStr :String!;
    
   public init(queryString: String) {
        self.queryStr = queryString;
    }
    
    private override init() {
        
    }
    
    //"https://api-v2launch.trakt.tv/search?limit=10&page=1&query=Ard&type=movie";
  public  func getImagesList(callback:(movies:[Movie]) -> Void) ->NSURLSessionDataTask? {
        let urlStr = BASE_URL + "?limit=\(self.mCount)&page=\(self.currentIndex)&query=\(self.queryStr)&type=movie"

        if isPending == false {
            isPending = true;
           return DownloadManager.sharedInstance.downloadJSON(urlStr) { (obj, data) in
                self.isPending = false;
                if data != nil {
                    
                    let arr = data as! NSArray;
                    var moviesArr:Array<Movie> = [];
                    
                    
                    for i in 0...(arr.count-1)  {
                        moviesArr.append(Movie(dictionary: arr[i] as! NSDictionary));
                    }
                    
                    
                    
                    callback(movies: moviesArr);
                }
                
                
                
            }
        }
        return nil;
    }
    
    //increment count by 1 for next set of image downloading
    func incrementCount(){
        self.currentIndex += 1;
    }
    
    
    
}


//Movie Bean for Response
public class Movie: NSObject {
    
   public var title:String!;
   public var poster:ImageUrl!;
   public var fanart:ImageUrl!;
    private override init() {
        
    }

    init(dictionary:NSDictionary) {
        
        let dict = dictionary["movie"] as! NSDictionary;
        self.title = dict["title"] as! String;
        let imgDict = dict["images"] as! NSDictionary;
        self.poster = ImageUrl(dictionary: imgDict["poster"] as! NSDictionary);
        self.fanart = ImageUrl(dictionary: imgDict["fanart"] as! NSDictionary);
    }
    
    
    
    /*
     
     
     "type": "movie",
     "score": 37.854153,
     "movie": {
     "title": "Forces of Nature",
     "overview": "Determined to scare The Once-ler out of Truffula Valley, The Lorax decides to create the illusion of ominous forces of nature.",
     "year": 2012,
     "images": {
     "poster": {
     "full": "https://walter.trakt.us/images/movies/000/086/251/posters/original/44fa217918.jpg",
     "medium": "https://walter.trakt.us/images/movies/000/086/251/posters/medium/44fa217918.jpg",
     "thumb": "https://walter.trakt.us/images/movies/000/086/251/posters/thumb/44fa217918.jpg"
     },
     "fanart": {
     "full": "https://walter.trakt.us/images/movies/000/086/251/fanarts/original/9dbe863d53.jpg",
     "medium": "https://walter.trakt.us/images/movies/000/086/251/fanarts/medium/9dbe863d53.jpg",
     "thumb": "https://walter.trakt.us/images/movies/000/086/251/fanarts/thumb/9dbe863d53.jpg"
     }
     },
     "ids": {
     "trakt": 86251,
     "slug": "forces-of-nature-2012",
     "imdb": "tt2584678",
     "tmdb": 125453
     }
     }
     
     */
}

public struct ImageUrl {
   public let fullUrlStr:String!;
   public let mediumStr : String!;
   public let thumbStr : String!;
    
    init(dictionary:NSDictionary) {
        
        self.fullUrlStr = dictionary["full"] as? String;
        self.mediumStr = dictionary["medium"] as? String;
        self.thumbStr = dictionary["thumb"] as? String;
        
    }
    
    
    
}
