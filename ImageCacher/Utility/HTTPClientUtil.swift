//
//  HTTPClientUtil.swift
//  BSDI-App
//
//  Created by Babul on 08/02/16.
//  Copyright © 2016 Babul. All rights reserved.
//

import Foundation
let BASIC_AUTH_USERNAME = "guest";
let BASIC_AUTH_PASSWORD = "guest";
/**
 Network Status Code
 
 - HTTP_SUCCESS_STATUS_CODE:    Used when Api response is success
 - INTERNET_CONNECTION_FAILURE: Used when Api response is failure due to internet connection
 */
enum HttpStatusCode:Int {
    
    case HTTP_SUCCESS_STATUS_CODE = 200
    case INTERNET_CONNECTION_FAILURE = -1
    case AUTH_TOKEN_EXPIRE = -2
    
}

/**
 ENUM For HTTP Methods
 
 - GET_HTTP_API:  GET HTTP METHOD
 - POST_HTTP_API: POST HTTP METHOD
 - PUT_HTTP_API:  PUT HTTP METHOD
 */
public enum HttpMethodType:String {
    
    case GET_HTTP_API = "GET"
    case POST_HTTP_API = "POST"
    case PUT_HTTP_API = "PUT"
    
}
extension Dictionary where Key: StringLiteralConvertible, Value: StringLiteralConvertible{
    
    
    /*url encoding*/
    func toURLEncodedString() -> NSString {
        
        let urlStr = NSMutableString();
        
        for (key,value) in self {
            
            let keyStr = ((key as! NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!);
            let valueStr = ((value as! NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!);
            urlStr.appendString("\(keyStr)=\(valueStr)&");
            
        }
        
        
        
        if urlStr.length > 0 {
            
            
            urlStr.deleteCharactersInRange(NSMakeRange((urlStr.length - 1), 1))
        }
        
        return urlStr ;
        
    }
    
    
    /**
     JSON Encoding for key value Pairs
     
     - returns: JSON String
     */
    func jsonEncodedKeyValueString() -> String? {
        
        let data:NSData = try! NSJSONSerialization.dataWithJSONObject((self as! AnyObject), options: NSJSONWritingOptions.PrettyPrinted)
        return NSString(data: data, encoding:NSUTF8StringEncoding) as? String
        
    }
    
    
    
}


public class HTTPClientUtil:NSObject,NSURLSessionDelegate, NSURLSessionTaskDelegate {
    
    
    class func requestHeaders() -> [String: String] {
        return [
            "content-type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "ad005b8c117cdeee58a1bdb7089ea31386cd489b21e14b19818c91511f12a086",
        ]
    }
    
    
    /**
     GET Api Call :Calls Get Api
     
     - parameter urlStr:         Consumed URlString
     - parameter requestParams:  request Url Params
     - parameter requestHeaders: Custom Headers
     - parameter callback:       Callback for Response Block
     */
    public  func httpGET(urlStr:String,requestParams:[String:String]?,requestHeaders:[String:String]?, callback:(obj:NSHTTPURLResponse?,data: NSData?) ->Void) ->NSURLSessionDataTask {
        
        if requestParams != nil {
            let urlParams = requestParams?.toURLEncodedString();
           return self.callApi(.GET_HTTP_API, urlStr: "\(urlStr)?\(urlParams)", requestParams: nil, requestHeaders: requestHeaders, callback: callback)
        } else {
           return self.callApi(.GET_HTTP_API, urlStr: "\(urlStr)", requestParams: nil, requestHeaders: requestHeaders, callback: callback)
        }
    }
    
    /**
     PUT Api Call :Calls Put Api
     
     - parameter urlStr:         Consumed URlString
     - parameter requestParams:  request Query Params
     - parameter requestHeaders: Custom Headers
     - parameter callback:       Callback for Response Block
     */
    public  func httpPUT(urlStr:String,requestParams:[String:String]?,requestHeaders:[String:String]?,callback:(obj:NSHTTPURLResponse?,data: NSData?) ->Void) ->NSURLSessionDataTask  {
        
       return self.callApi(.PUT_HTTP_API, urlStr: urlStr, requestParams: requestParams, requestHeaders: requestHeaders, callback: callback)
    }
    
    /**
     POST Api Call :Calls Post Api
     
     - parameter urlStr:         Consumed URlString
     - parameter requestParams:  request Query Params
     - parameter requestHeaders: Custom Headers
     - parameter callback:       Callback for Response Block
     */
    public  func httpPOST(urlStr:String,requestParams:[String:String]?,requestHeaders:[String:String]?,callback:(obj:NSHTTPURLResponse?,data: NSData?) ->Void) ->NSURLSessionDataTask {
        
       return self.callApi(.POST_HTTP_API, urlStr: urlStr, requestParams: requestParams, requestHeaders: requestHeaders, callback: callback)
    }
    
    /**
     Api Call
     
     - parameter methodType:     HTTP Method Type
     - parameter urlStr:         Consumed URlString
     - parameter requestParams:  request Query Params
     - parameter requestHeaders: Custom Headers
     - parameter callback:       Callback for Response Block
     
     */
    public func callApi(methodType:HttpMethodType,urlStr:String,requestParams:[String:String]?,requestHeaders:[String:String]?,callback:(obj:NSHTTPURLResponse?,data: NSData?) ->Void) ->NSURLSessionDataTask {
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration(); //default Session Configuration for NSUrlSession
        configuration.HTTPMaximumConnectionsPerHost = 10;
        let session = NSURLSession(configuration: configuration,
                                   delegate: self,
                                   delegateQueue:DownloadManager.sharedInstance.operationQueue) //create NSURLSession with delegates implemented by self
       
        let request = NSMutableURLRequest(URL: NSURL(string: urlStr)!);//Create MutableUrlRequest
        request.HTTPMethod = methodType.rawValue; //get method type raw value
       
        
        
        if requestHeaders != nil { //check if Custom Headers are present if yes add to request type
            for (key,value) in requestHeaders! {
                
                request.setValue(value, forHTTPHeaderField: key);
            }
        }
        if requestParams != nil  {//Encode Request Params to JSON Encoded Key Value String
            let requestParamsStr = requestParams?.toURLEncodedString();
            request.HTTPBody = requestParamsStr?.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
    
        let task = session.dataTaskWithRequest(request) { (data, response, error)  -> Void in
            
            defer {
                
                callback(obj: response as? NSHTTPURLResponse, data: data);
                
            }
            
        };
        
        
        task.resume(); //call api
        return task;
    }
    
    //MARK: - URL Session Delegates -
    
    public func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        completionHandler(NSURLSessionAuthChallengeDisposition.UseCredential,NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!));
    }
    
    
    
    public func URLSession(session: NSURLSession, task: NSURLSessionTask, willPerformHTTPRedirection response: NSHTTPURLResponse, newRequest request: NSURLRequest, completionHandler: (NSURLRequest?) -> Void) {
        let newRequest : NSURLRequest? = request
        print(newRequest?.description);
        completionHandler(newRequest)
    }
    
    
}